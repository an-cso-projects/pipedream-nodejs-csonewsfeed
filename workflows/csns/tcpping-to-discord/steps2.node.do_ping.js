const schema = {
    "type": "object",
    "properties": {
        "servers": {
            "type": "array",
            "items": {
                "type": "string"
            },
            "_label": "Server List (host:port)",
            "example": "127.0.0.1:1234"
        },
        "attempts": {
            "type": "integer",
            "_label": "Ping Attemps",
            "example": "5"
        },
        "timeout": {
            "type": "integer",
            "_label": "Ping Timeout (in millisecond unit)",
            "example": "5000"
        }
    },
    "required": [
        "servers"
    ]
};
const params = {
    servers: [
        "52.28.177.60:8006",
        "52.28.118.112:8005",
        "52.28.186.122:8007",
        "52.28.20.129:8008",
    ],
};
const code = async function(event, steps, params) {
    const _ = require('lodash');
    const tcpPing = require('util').promisify(require('tcp-ping').ping);
    const hash = require('object-hash');

    // Required.
    const servers = params.servers;

    // Optional.
    const attempts = params.attempts || 1;
    const timeout = params.timeout || null;

    const promises = await Promise.allSettled(
      _(servers)
      .map((e) => e.split(':'))
      .map((e) => tcpPing({address:e[0], port:parseInt(e[1]), attempts:attempts, timeout:timeout,})
        .then((response) => {
          const results = response.results;
          if (_.isEmpty(results)) throw Error('No result');
          const badResults = _.filter(results, 'err');
          const goodResults = _.difference(results, badResults);
          const lastBad = _.last(badResults);
          if (_.isEmpty(goodResults)) throw lastBad.err;
          return response;
        })
        .catch( (reason) => ({
          address: e[0],
          port: parseInt(e[1]),
          err: reason.toString(),
        }))
      )
      .value()
    );

    const response = _(promises)
    .map('value')
    .flatten()
    .compact()
    .value();

    $checkpoint = $checkpoint || {};
    const cpRaw = _.map( response, (e)=>_.pick(e, ['address', 'port', 'err']) );
    const cpHash = hash( cpRaw );
    console.log( cpHash, cpRaw );
    if (_.isEqual( $checkpoint.hash, cpHash )) {
      if ($end) $end( 'No update' );
    }
    $checkpoint.hash = cpHash;

    return _(response).map((e,i) => {
      return [
        i <= 0 ? null : {
          name: '\u200b',
          value: '\u200b',
          inline: false,
        }, {
          name: 'Host',
          value: `${e.address}:${e.port}`,
          inline: true,
        }, {
          name: 'Status',
          value: e.err || `**ONLINE**: ${_.round(e.avg)}ms`,
          inline: true,
        }
      ]
    }).flatten().compact().value();
};