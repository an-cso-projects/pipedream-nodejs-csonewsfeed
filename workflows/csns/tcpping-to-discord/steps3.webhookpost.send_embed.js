const headers = {
    'Content-Type': 'application/json',
}
const body =
{
    "embeds": [{
        "title": "CSNS Master Server",
        "type": "rich",
        "timestamp": "{{steps.trigger.context.ts}}",
        "fields": {{JSON.stringify( steps.do_ping.$return_value )}}
    }]
}