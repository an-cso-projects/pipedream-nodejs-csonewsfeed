const schema = {
    "type": "object",
    "properties": {
        "url": {
            "type": "string"
        },
        "queries": {
            "type": "object"
        },
        "headers": {
            "type": "object"
        }
    },
    "required": [
        "url"
    ]
};
const params = {
    url: 'https://shot.screenshotapi.net/screenshot',
    headers: {
        Accept: 'application/json',
    },
    queries: {
        token: '<your SS token>',
        url: '{{event.link}}',
        width: '768',
        height: '1024',
        scroll_to_element: 'div.EventDetail',
    },
};
const code = async function(event, steps, params) {
    const axios = require('axios');


    // Required.
    const link = params.url;
    // Optional.
    const queries = Object.assign({
        output: 'json',
        wait_for_event: 'load',
    }, params.queries);
    const headers = params.headers;


    return axios.get(link, {headers:headers,params:queries,})
    .then((response) => response.data)
    .catch((reason) => {
        console.error(new String(reason).toString());
        return {
            error: reason,
        };
    });
};