const schema = {
    "type": "object",
    "properties": {
        "reason": {
            "type": "string"
        }
    },
    "required": []
};
const params = {
    reason: 'A possibly duplicate entry.',
};
const code = async function(event, steps, params) {
    if (/^A lil somethin somethin:.*\[url=.*\].*\[\/url\].$/gi.exec(event.description))
        $end(params.reason)
};