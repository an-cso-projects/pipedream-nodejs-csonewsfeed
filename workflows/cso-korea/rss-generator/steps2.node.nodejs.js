const schema = {
    "type": "object",
    "properties": {
        "rssTitle": {
            "type": "string",
            "_label": "Title of this RSS",
            "example": "My Web RSS"
        },
        "rssSourceLink": {
            "type": "string",
            "_label": "Source URL to Track",
            "example": "https://example.com"
        },
        "rssLang": {
            "type": "string",
            "_label": "Language Code",
            "example": "en-us"
        },
        "rssTimezone": {
            "type": "string",
            "_label": "Time Zone Offset",
            "example": "+09.00"
        }
    },
    "required": [
        "rssTitle",
        "rssSourceLink"
    ]
};
const params = {
    rssTitle: 'CSO Korea News',
    rssSourceLinks: 'https://csonline.nexon.com/News/Notice/List',
    rssLang: 'ko-kr',
    rssTimezone: '+09:00',
};
const code = async function(event, steps, params) {
    const axios = require( 'axios' ).default;
    const cheerio = require( 'cheerio' );
    const _ = require( 'lodash' );
    const Feed = require('feed').Feed;

    // required.
    const rssTitle = params.rssTitle;
    const rssSourceLink = params.rssSourceLink;

    // optional.
    const rssLang = _.lowerCase( params.rssLang || 'en-us' );
    const rssTimezone = params.rssTimezone || 'Z';

    const feed = new Feed( {
        id: rssSourceLink,
        title: rssTitle,
        language: rssLang,
        link: rssSourceLink,
        description: 'AnggaraNothing',
        updated: new Date(),
        ttl: 5,
        copyright: 'AnggaraNothing'
    } );

    await axios.get( rssSourceLink )
    .then( (response) => {
        const $ = cheerio.load( response.data );
        $('li', 'ul.list._skin4').each( (i, el) => {
            const title = $( 'a', 'span.noti_tit', el );
            const link = new URL( title.attr('href'), rssSourceLink ).toString();
            const category = $('span.noti_cate', el).text().replace('"', '');
            const content = $('a', 'span.noti_con', el).text();
            let pubdate = (new Date()).toISOString().replace( /Z|[+-]\d{2}\.\d{2}/i, rssTimezone );
            const datetimeTxt = $('span.time', el).text();
            const timeRegex = /\d{2}:\d{2}:\d{2}(.\d+)?/i;
            if (datetimeTxt.match(/([AP]M) (\d{2}.\d{2})/i)) {
                let [modifier, hours, minutes] = datetimeTxt.split(/\W+/);

                if (hours === '12') {
                    hours = '00';
                }

                if (_.upperCase(modifier) === 'PM') {
                    hours = parseInt(hours, 10) + 12;
                }

                pubdate = Promise.resolve( new Date(pubdate.replace(timeRegex, `${hours}:${minutes}:00`)) );
            } else {
                const oldPubDate = pubdate;
                const dateRegex = /(\d{4})\.(\d{2})\.(\d{2})/i;
                pubdate = axios.get( link )
                    .then( (response) => {
                        const $ = cheerio.load( response.data );
                        const datetimeTxt = $('span.time').text();
                        return new Date(
                            oldPubDate
                                .replace( timeRegex, /\d{2}:\d{2}/i.exec(datetimeTxt) + ':00' )
                                .replace( /(\d{4})\-(\d{2})\-(\d{2})/i, dateRegex.exec(datetimeTxt).slice(1).join('-') )
                        );
                    })
                    .catch( (reason) => {
                        console.error( reason.toString() );
                        return new Date(
                            oldPubDate
                                .replace( timeRegex, `18:00:00` )
                                .replace( /(\d{4})\-(\d{2})\-(\d{2})/i, dateRegex.exec(datetimeTxt).slice(1).join('-') )
                        );
                    })
            }

            feed.items[i] = {
                id: link,
                link: link,
                title: title.text(),
                date: pubdate,
                published: pubdate,
                description: content,
                content: content,
                category: [
                    {
                        name: category,
                    }
                ],
            };
        } )
    } )
    .then( async () => {
        _(await _(feed.items).map('date').thru((e)=>Promise.allSettled(e)).value())
            .map((e)=>({date: e.value, published: e.value}))
            .each((e,i) => Object.assign(feed.items[i],e));
    });

    await $respond({
        immediate: true,
        status: 200,
        headers: { "Content-Type": "application/atom+xml" },
        body: feed.atom1(),
    });
};