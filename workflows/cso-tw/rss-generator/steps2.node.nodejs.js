const schema = {
    "type": "object",
    "properties": {
        "rssTitle": {
            "type": "string",
            "_label": "Title of this RSS",
            "example": "My Web RSS"
        },
        "rssSourceLink": {
            "type": "string",
            "_label": "Source URL to Track",
            "example": "https://example.com"
        },
        "rssLang": {
            "type": "string",
            "_label": "Language Code",
            "example": "en-us"
        }
    },
    "required": [
        "rssTitle",
        "rssSourceLink"
    ]
};
const params = {
    rssTitle: 'CSO Taiwan & Hong Kong News',
    rssSourceLinks: 'https://tw.beanfun.com/cso/Bulletins/include/Bulletins_Proxy.aspx?ServiceType=128&alt=0&Page=1&method=0&Kind=0&Pagesize=10&toAll=1',
    rssLang: 'zh-TW',
};
const code = async function(event, steps, params) {
    const axios = require( 'axios' ).default;
    const _ = require( 'lodash' );
    const Feed = require('feed').Feed;

    // required.
    const rssTitle = params.rssTitle;
    const rssSourceLink = params.rssSourceLink;

    // optional.
    const rssLang = _.lowerCase( params.rssLang || 'en-us' );

    const feed = new Feed( {
        id: rssSourceLink,
        title: rssTitle,
        language: rssLang,
        link: rssSourceLink,
        description: 'AnggaraNothing',
        updated: new Date(),
        ttl: 5,
        copyright: 'AnggaraNothing'
    } );

    await axios.get( rssSourceLink )
    .then( (response) => {
        _.each( response.data.MyDataSet.Table, (val, i) => {
            const link = new URL(val.UrlLink ? val.UrlLink : `https://cso.beanfun.com/news/detail.html?Bid=${val.BullentinId}`, rssSourceLink).toString();
            const title = val.Title;
            const pubdate = new Date( val.StartDate );
            const content = JSON.stringify( val );
            const category = /^【(.+?)】/i.exec( title );
            feed.items[i] = {
                id: link,
                link: link,
                title: title,
                date: pubdate,
                published: pubdate,
                description: content,
                content: content,
                category: [
                    {
                        name: category ? category[1] : val.BullentinCatId,
                    }
                ],
            };
        } );
    } );

    await $respond({
        immediate: true,
        status: 200,
        headers: { "Content-Type": "application/atom+xml" },
        body: feed.atom1(),
    });
};