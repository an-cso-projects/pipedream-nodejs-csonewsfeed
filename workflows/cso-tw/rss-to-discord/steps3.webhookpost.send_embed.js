const headers = {
    'Content-Type': 'application/json',
}
const body =
{
    "embeds": [{
        "title": {{ JSON.stringify(event.title) }},
        "type": "rich",
        "url": {{ JSON.stringify(event.link) }},
        "timestamp":  {{ JSON.stringify(event["atom:published"]["#"]) }},
        "image": {
            "url": {{ JSON.stringify(steps.do_ss.$return_value?.screenshot ? steps.do_ss.$return_value.screenshot : null) }}
        }
    }]
}