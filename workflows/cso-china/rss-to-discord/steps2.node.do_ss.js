const schema = {
    "type": "object",
    "properties": {
        "url": {
            "type": "string"
        },
        "queries": {
            "type": "object"
        },
        "headers": {
            "type": "object"
        }
    },
    "required": [
        "url"
    ]
};
const params = {
    url: 'https://shot.screenshotapi.net/screenshot',
    headers: {
        Accept: 'application/json',
    },
    queries: {
        token: '<your SS token>',
        url: '{{event.link}}',
        width: '690',
        height: '1024',
        scroll_to_element: 'div.newsshow, div#mod_article, div#content, div.index-leftWrapper-DI63v',
        css: 'div#qreg,div.gb-final-side{width: 0;visibility: hidden}div.article-mod-media,div.btn-article-media,div.gb-newgame-comm{height: 0;visibility: hidden}div#global_header_nav,div.div.left_side-leftSideBar-Nh5Uh,div.left_side-leftSideBar-Nh5Uh,div.index-rightWrapper-b0XVG,div.web-message-wrapper{height: 0;width: 0;visibility: hidden;padding: unset}div.app-mainWrapper-q1qYv,div.index-wrapper-8URc7{padding: unset;margin: unset;height: auto;width: auto}div.index-leftWrapper-DI63v{width: auto;float: none}div#content,div.gb-final-main{width: 680px;margin: unset;padding: 0 5px 0 5px}',
    },
};
const code = async function(event, steps, params) {
    const axios = require('axios');


    // Required.
    const link = params.url;
    // Optional.
    const queries = Object.assign({
        output: 'json',
        wait_for_event: 'load',
    }, params.queries);
    const headers = params.headers;


    return axios.get(link, {headers:headers,params:queries,})
    .then((response) => response.data)
    .catch((reason) => {
        console.error(new String(reason).toString());
        return {
            error: reason,
        };
    });
};