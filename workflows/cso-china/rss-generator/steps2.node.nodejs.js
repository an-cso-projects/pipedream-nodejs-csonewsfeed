const schema = {
    "type": "object",
    "properties": {
        "rssTitle": {
            "type": "string",
            "_label": "Title of this RSS",
            "example": "My Web RSS"
        },
        "rssSourceLinks": {
            "type": "array",
            "_label": "Source URL(s) to Track",
            "example": "https://example.com",
            "items": {
                "type": "string"
            }
        },
        "rssItemsPerLink": {
            "type": "integer",
            "_label": "Max Item Size per Source",
            "example": "5"
        },
        "rssLang": {
            "type": "string",
            "_label": "Language Code",
            "example": "en-us"
        },
        "rssTimezone": {
            "type": "string",
            "_label": "Time Zone Offset",
            "example": "+09.00"
        }
    },
    "required": [
        "rssTitle",
        "rssSourceLinks"
    ]
};
const params = {
    rssTitle: 'CSO China News',
    rssSourceLinks: [
        'https://csol.tiancity.com/homepage/article/Class_2_Time_1.html',
        'https://csol.tiancity.com/homepage/article/Class_7_Time_1.html',
        'https://csol.tiancity.com/homepage/article/Class_6_Time_1.html',
    ],
    rssLang: 'zh-CN',
    rssTimezone: '+08:00',
};
const code = async function(event, steps, params) {
    const axios = require( 'axios' ).default;
    const cheerio = require( 'cheerio' );
    const _ = require( 'lodash' );
    const Feed = require('feed').Feed;
    const iconv = require('iconv-lite');

    // required.
    const rssTitle = params.rssTitle;
    const rssSourceLinks = params.rssSourceLinks;

    // optional.
    const rssItemsPerLink = params.rssItemsPerLink || 5;
    const rssLang = _.lowerCase( params.rssLang || 'en-us' );
    const rssTimezone = params.rssTimezone || 'Z';

    const hubSource = 'https://csol.tiancity.com/homepage/article/Class_4_Time_1.html';
    const feed = new Feed( {
        id: hubSource,
        link: hubSource,
        title: rssTitle,
        language: rssLang,
        description: 'AnggaraNothing',
        updated: new Date(),
        ttl: 5,
        copyright: 'AnggaraNothing'
    } );

    const requests = _(rssSourceLinks)
    .map((e) => axios.get(e, {responseType: 'arraybuffer'})
        .then((response) => {
            let html;
            try {
                html = iconv.decode( response.data, 'gb2312' );
            } catch (e) {
                console.error( e );
                html = iconv.decode( response.data, 'utf-8' );
            }
            const $ = cheerio.load( html );
            $('li', '.newslists').each( (i, el) => {
                if (i >= rssItemsPerLink) return false;

                const title = $( 'a.news_title', el );
                const link = new URL( title.attr('href'), e ).toString();
                const category = $('a.news_classify', el).text();
                const content = $('em', el).text();
                const dateRegex = /(\d{4})\-(\d{2})\-(\d{2})/i;
                const pubdate = new Date(
                    (new Date())
                    .toISOString()
                    .replace( /Z|[+-]\d{2}\.\d{2}/i, rssTimezone )
                    .replace( /\d{2}:\d{2}:\d{2}(.\d+)?/i, `18:00:00` )
                    .replace( dateRegex, dateRegex.exec(content)[0] )
                );

                feed.items.push({
                    id: link,
                    link: link,
                    title: title.text(),
                    date: pubdate,
                    published: pubdate,
                    description: content,
                    content: content,
                    category: [
                        {
                            name: category,
                        }
                    ],
                });
            } );
        })
    )
    .value();

    await Promise.allSettled(requests)
    .finally(() => {
        feed.items = _(feed.items).sortBy(['date','title','id']).reverse().value();
    });

    await $respond({
        immediate: true,
        status: 200,
        headers: { "Content-Type": "application/atom+xml" },
        body: feed.atom1(),
    });
};